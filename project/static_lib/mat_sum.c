#include <stdlib.h>
#include "mat_sum.h"

static bool sum_cols(const matrix *mat, double *res);

static bool sum_rows(const matrix *mat, double *res);

bool transpose_matrix(matrix **new_mat, const matrix *old_mat) {
    if (!new_mat || !old_mat) {
        return false;
    }
    *new_mat = (matrix *) malloc(sizeof(matrix));
    if (!*new_mat) {
        return false;
    }
    matrix *mat = *new_mat;
    mat->n_cols = old_mat->n_rows;
    mat->n_rows = old_mat->n_cols;
    mat->arr = (double **) malloc(mat->n_rows * sizeof(double *));
    if (!mat->arr) {
        return false;
    }
    for (size_t i = 0; i < mat->n_rows; ++i) {
        mat->arr[i] = (double *) malloc(mat->n_cols * sizeof(double));
        if (!mat->arr[i]) {
            return false;
        }
    }
    for (size_t i = 0; i < mat->n_rows; ++i) {
        for (size_t j = 0; j < mat->n_cols; ++j) {
            mat->arr[i][j] = old_mat->arr[j][i];
        }
    }
    return true;
}

static bool sum_cols(const matrix *mat, double *res) {
    if (!mat || !res) {
        return false;
    }
    for (size_t j = 0; j < mat->n_cols; ++j) {
        res[j] = 0;
        for (size_t i = 0; i < mat->n_rows; ++i) {
            res[j] += mat->arr[i][j];
        }
    }
    return true;
}

static bool sum_rows(const matrix *mat, double *res) {
    if (!mat || !res) {
        return false;
    }
    for (size_t i = 0; i < mat->n_rows; ++i) {
        res[i] = 0;
        for (size_t j = 0; j < mat->n_cols; ++j) {
            res[i] += mat->arr[i][j];
        }
    }
    return true;
}

bool matrix_sum_elems(const matrix *mat, double *res, int axis) {
    if (!mat || !res || (axis != 0 && axis != 1)) {
        return false;
    }
    if (mat->n_rows <= 0 || mat->n_cols <= 0) {
        return false;
    }
    if (axis == 0) {
        if (!sum_cols(mat, res)) {
            return false;
        }
    } else {
        if (!sum_rows(mat, res)) {
            return false;
        }
    }
    return true;
}

