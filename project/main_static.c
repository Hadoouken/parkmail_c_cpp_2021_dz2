#define _POSIX_C_SOURCE 199309L

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mat_sum.h"

#define RANDOM_SEED 42
#define RETRIES 10

void bench_static_lib(const int n_rows, const int n_cols, const char *path) {
    matrix *mat;
    double *res = (double *) malloc(n_cols * sizeof(double));
    if (!res) {
        return;
    }
    if (!init_random_matrix(&mat, n_rows, n_cols, RANDOM_SEED)) {
        free(res);
        return;
    }
    printf("1. Benchmarking original matrix...\n");
    struct timespec start, finish;
    double avg_time;
    clock_gettime(CLOCK_MONOTONIC, &start);
    for (size_t i = 0; i < RETRIES; ++i) {
        if (!matrix_sum_elems(mat, res, 0)) {
            free(res);
            del_matrix(mat);
            return;
        }
    }
    clock_gettime(CLOCK_MONOTONIC, &finish);
    avg_time = (finish.tv_sec - start.tv_sec);
    avg_time += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    avg_time = avg_time / RETRIES;
    printf("Average sum time spent: %f\n", avg_time);

    printf("2. Benchmarking transpose matrix...\n");
    clock_gettime(CLOCK_MONOTONIC, &start);
    matrix *mat_t;
    if (!transpose_matrix(&mat_t, mat)) {
        free(res);
        del_matrix(mat);
        del_matrix(mat_t);
        return;
    }
    clock_gettime(CLOCK_MONOTONIC, &finish);
    avg_time = (finish.tv_sec - start.tv_sec);
    avg_time += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    printf("Transposing matrix time spent: %f\n", avg_time);
    clock_gettime(CLOCK_MONOTONIC, &start);
    for (size_t i = 0; i < RETRIES; ++i) {
        if (!matrix_sum_elems(mat_t, res, 1)) {
            free(res);
            del_matrix(mat);
            del_matrix(mat_t);
            return;
        }
    }
    clock_gettime(CLOCK_MONOTONIC, &finish);
    avg_time = (finish.tv_sec - start.tv_sec);
    avg_time += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    avg_time = avg_time / RETRIES;
    printf("Average sum time spent: %f\n", avg_time);

    FILE *fp;
    fp = fopen(path, "w");
    print_array(res, n_cols, fp);
    fclose(fp);

    free(res);
    del_matrix(mat_t);
    del_matrix(mat);
}

int main(int argc, char *argv[]) {
    const int n_rows = (int) strtol(argv[1], NULL, 10);
    const int n_cols = (int) strtol(argv[2], NULL, 10);
    printf("Benchmarking matrix elements sum by column for static lib...\n");
    printf("Matrix size: n_rows = %d, n_cols = %d\n", n_rows, n_cols);
    bench_static_lib(n_rows, n_cols, argv[3]);
    return 0;
}
