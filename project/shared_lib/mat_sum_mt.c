#include <pthread.h>
#include <stdlib.h>
#include "mat_sum_mt.h"

typedef struct arg_struct {
    size_t threads_num;
    int id;
    const matrix *mat;
    double *res;
} arg_struct;

static bool init_args(arg_struct *args, const matrix *mat, double *res,
                      size_t threads_num);

static void del_args(arg_struct *arg, size_t threads_num);

static void *thread_sum_cols(void *arg);

static void *thread_sum_rows(void *arg);

static bool init_args(arg_struct *args, const matrix *mat, double *res,
               size_t threads_num) {
    if (!args || !mat || !res || threads_num <= 0) {
        return false;
    }
    for (size_t i = 0; i < threads_num; ++i) {
        args[i].id = i;
        args[i].threads_num = threads_num;
        args[i].mat = mat;
        args[i].res = res;
    }
    return true;
}

bool transpose_matrix(matrix **new_mat, const matrix *old_mat) {
    if (!new_mat || !old_mat) {
        return false;
    }
    *new_mat = (matrix *) malloc(sizeof(matrix));
    if (!*new_mat) {
        return false;
    }
    matrix *mat = *new_mat;
    mat->n_cols = old_mat->n_rows;
    mat->n_rows = old_mat->n_cols;
    mat->arr = (double **) malloc(mat->n_rows * sizeof(double *));
    if (!mat->arr) {
        return false;
    }
    for (size_t i = 0; i < mat->n_rows; ++i) {
        mat->arr[i] = (double *) malloc(mat->n_cols * sizeof(double));
        if (!mat->arr[i]) {
            return false;
        }
    }
    for (size_t i = 0; i < mat->n_rows; ++i) {
        for (size_t j = 0; j < mat->n_cols; ++j) {
            mat->arr[i][j] = old_mat->arr[j][i];
        }
    }
    return true;
}

static void *thread_sum_cols(void *arg) {
    arg_struct args = *((arg_struct *) arg);
    size_t num_of_cols_to_calculate =
            (args.mat->n_cols - args.id + (args.threads_num - 1)) /
            args.threads_num;
    int col_index = args.id;
    for (size_t i = 0; i < num_of_cols_to_calculate; ++i) {
        args.res[col_index] = 0;
        for (size_t row = 0; row < args.mat->n_rows; row++) {
            args.res[col_index] += args.mat->arr[row][col_index];
        }
        col_index += args.threads_num;
    }
    return NULL;
}

static void *thread_sum_rows(void *arg) {
    arg_struct args = *((arg_struct *) arg);
    size_t num_of_rows_to_calculate =
            (args.mat->n_rows - args.id + (args.threads_num - 1)) /
            args.threads_num;
    int row_index = args.id;
    for (size_t i = 0; i < num_of_rows_to_calculate; ++i) {
        args.res[row_index] = 0;
        for (size_t col = 0; col < args.mat->n_cols; col++) {
            args.res[row_index] += args.mat->arr[row_index][col];
        }
        row_index += args.threads_num;
    }
    return NULL;
}

bool matrix_sum_elems(const matrix *mat, double *res, int axis,
                      size_t threads_num) {
    if (!mat || !res || threads_num <= 0 || (axis != 0 && axis != 1)) {
        return false;
    }
    if (mat->n_rows <= 0 || mat->n_cols <= 0) {
        return false;
    }
    pthread_t *threadIds = (pthread_t *) malloc(
            threads_num * sizeof(pthread_t));
    if (!threadIds) {
        return false;
    }
    arg_struct *args = (arg_struct *) malloc(threads_num * sizeof(arg_struct));
    if (!args) {
        free(threadIds);
        return false;
    }
    if (!init_args(args, mat, res, threads_num)) {
        free(threadIds);
        del_args(args, threads_num);
        return false;
    }
    void *(*calc_func)(void *);
    if (axis == 0) {
        calc_func = &thread_sum_cols;
    } else {
        calc_func = &thread_sum_rows;
    }
    for (size_t i = 0; i < threads_num; ++i) {
        int errflag = pthread_create(&threadIds[i], NULL, *calc_func,
                                     (void *) &args[i]);
        if (errflag != 0) {
            free(threadIds);
            del_args(args, threads_num);
            return false;
        }
    }
    for (size_t i = 0; i < threads_num; ++i) {
        int errflag = pthread_join(threadIds[i], NULL);
        if (errflag != 0) {
            free(threadIds);
            del_args(args, threads_num);
            return false;
        }
    }
    free(threadIds);
    del_args(args, threads_num);
    return true;
}

static void del_args(arg_struct *arg, size_t threads_num) {
    if (!arg) {
        return;
    }
    for (size_t i = 0; i < threads_num; ++i) {
        arg[i].mat = NULL;
        arg[i].res = NULL;
    }
    free(arg);
}
