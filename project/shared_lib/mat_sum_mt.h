#pragma once

#include <stdbool.h>
#include "utils.h"

bool transpose_matrix(matrix **new_mat, const matrix *old_mat);

bool matrix_sum_elems(const matrix *mat, double *res, int axis,
                      size_t threads_num);
