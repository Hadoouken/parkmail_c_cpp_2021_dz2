#pragma once

#include <stdbool.h>
#include <stdio.h>

typedef struct {
    size_t n_cols;
    size_t n_rows;
    double **arr;
} matrix;

bool init_matrix_from_file(FILE *fp, matrix **mat_to_init);

bool init_matrix(matrix **mat_to_init, size_t n_rows, size_t n_cols);

size_t init_array_from_file(FILE *fp, double **arr);

bool compare_arrays(const double *arr1, const double *arr2, size_t size);

bool init_random_matrix(matrix **mat_to_init, size_t n_rows, size_t n_cols,
                        int seed);

void print_matrix(const matrix *mat, FILE *stream);

void print_array(const double *arr, size_t size, FILE *stream);

void del_matrix(matrix *mat);
