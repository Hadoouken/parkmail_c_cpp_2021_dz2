#include <stdlib.h>
#include "utils.h"

bool init_matrix_from_file(FILE *fp, matrix **mat_to_init) {
    if (!mat_to_init) {
        return false;
    }
    *mat_to_init = (matrix *) malloc(sizeof(matrix));
    if (!*mat_to_init) {
        return false;
    }
    matrix *mat = *mat_to_init;
    size_t n_rows, n_cols;
    if (fscanf(fp, "%ld", &n_rows) < 0) {
        return false;
    }
    if (fscanf(fp, "%ld", &n_cols) < 0) {
        return false;
    }
    mat->n_rows = n_rows;
    mat->n_cols = n_cols;
    mat->arr = (double **) malloc(mat->n_rows * sizeof(double *));
    if (!mat->arr) {
        return false;
    }
    for (size_t i = 0; i < mat->n_rows; ++i) {
        mat->arr[i] = (double *) malloc(mat->n_cols * sizeof(double));
        if (!mat->arr[i]) {
            return false;
        }
    }
    for (size_t i = 0; i < n_rows; ++i) {
        for (size_t j = 0; j < n_cols; ++j) {
            if (fscanf(fp, "%lf", &mat->arr[i][j]) < 0) {
                return false;
            }
        }
    }
    return true;
}

bool init_matrix(matrix **mat_to_init, size_t n_rows, size_t n_cols) {
    if (!mat_to_init || n_rows <= 0 || n_cols <= 0) {
        return false;
    }
    *mat_to_init = (matrix *) malloc(sizeof(matrix));
    if (!*mat_to_init) {
        return false;
    }
    matrix *mat = *mat_to_init;
    mat->n_cols = n_rows;
    mat->n_rows = n_cols;
    mat->arr = (double **) malloc(mat->n_rows * sizeof(double *));
    if (!mat->arr) {
        return false;
    }
    for (size_t i = 0; i < mat->n_rows; ++i) {
        mat->arr[i] = (double *) malloc(mat->n_cols * sizeof(double));
        if (!mat->arr[i]) {
            return false;
        }
    }
    return true;
}

size_t init_array_from_file(FILE *fp, double **arr) {
    if (!arr) {
        return -1;
    }
    size_t n_elems;
    if (fscanf(fp, "%ld", &n_elems) < 0) {
        return -1;
    }
    *arr = (double *) malloc(n_elems * sizeof(double));
    if (!*arr) {
        return -1;
    }
    double *arr2 = *arr;
    for (size_t i = 0; i < n_elems; ++i) {
        if (fscanf(fp, "%lf", &arr2[i]) < 0) {
            return -1;
        }
    }
    return n_elems;
}

bool compare_arrays(const double *arr1, const double *arr2, size_t size) {
    for (size_t i = 0; i < size; ++i) {
        if (arr1[i] != arr2[i]) {
            return false;
        }
    }
    return true;
}

bool init_random_matrix(matrix **mat_to_init, size_t n_rows, size_t n_cols,
                        int seed) {
    if (!mat_to_init || n_cols <= 0 || n_rows <= 0) {
        return false;
    }
    *mat_to_init = (matrix *) malloc(sizeof(matrix));
    if (!*mat_to_init) {
        return false;
    }
    matrix *mat = *mat_to_init;
    mat->n_cols = n_cols;
    mat->n_rows = n_rows;
    mat->arr = (double **) malloc(mat->n_rows * sizeof(double *));
    if (!mat->arr) {
        return false;
    }
    for (size_t i = 0; i < mat->n_rows; ++i) {
        mat->arr[i] = (double *) malloc(mat->n_cols * sizeof(double));
        if (!mat->arr[i]) {
            return false;
        }
    }
    srand(seed);
    for (size_t i = 0; i < mat->n_rows; ++i) {
        for (size_t j = 0; j < mat->n_cols; ++j) {
            mat->arr[i][j] = rand() % 10 + 1;
        }
    }
    return true;
}

void print_matrix(const matrix *mat, FILE *stream) {
    if (!mat) {
        return;
    }
    fprintf(stream, "%zu\r\n", mat->n_rows);
    fprintf(stream, "%zu\r\n", mat->n_cols);
    for (size_t i = 0; i < mat->n_rows; ++i) {
        for (size_t j = 0; j < mat->n_cols; ++j) {
            fprintf(stream, "%f", mat->arr[i][j]);
            if (j + 1 != mat->n_cols) {
                fprintf(stream, "\t");
            }
        }
        fprintf(stream, "\r\n");
    }
}

void print_array(const double *arr, size_t size, FILE *stream) {
    if (!arr) {
        return;
    }
    fprintf(stream, "%zu\r\n", size);
    for (size_t i = 0; i < size; ++i) {
        fprintf(stream, "%f", arr[i]);
        if (i + 1 != size) {
            fprintf(stream, "\t");
        }
    }
    fprintf(stream, "\r\n");
}

void del_matrix(matrix *mat) {
    if (!mat) {
        return;
    }
    for (size_t i = 0; i < mat->n_rows; ++i) {
        if (mat->arr[i]) {
            free(mat->arr[i]);
        }
    }
    if (mat->arr) {
        free(mat->arr);
    }
    free(mat);
}
