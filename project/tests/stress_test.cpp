#include <unistd.h>
#include <gtest/gtest.h>
#include <sstream>

#define BUFF_SIZE 128
#define RANDOM_SEED 42

extern "C" {
#include "utils.h"
}

TEST(StressTest, test_results) {
    int pid, status;
    char char_n_rows[BUFF_SIZE], char_n_cols[BUFF_SIZE];
    const size_t n_rows = 10000, n_cols = 5000;
    char path_res1[] = "../project/tests/test_data/static_stress_test_out.txt";
    char path_res2[] = "../project/tests/test_data/shared_stress_test_out.txt";
    auto *res1 = (double *) malloc(n_cols * sizeof(double));
    auto *res2 = (double *) malloc(n_cols * sizeof(double));
    snprintf(char_n_rows, BUFF_SIZE, "%zu", n_rows);
    snprintf(char_n_cols, BUFF_SIZE, "%zu", n_cols);
    pid = fork();
    if (pid == 0) {
        execl("./main_static.out", "./main_static.out", char_n_rows,
              char_n_cols, path_res1, NULL);
        exit(EXIT_SUCCESS);
    }
    pid_t waited_pid = waitpid(pid, &status, WNOHANG);
    while (!waited_pid) {
        waited_pid = waitpid(pid, &status, WNOHANG);
        sleep(1);
    }
    pid = fork();
    if (pid == 0) {
        execl("./main_shared.out", "./main_shared.out", char_n_rows,
              char_n_cols, path_res2, NULL);
        exit(EXIT_SUCCESS);
    }
    waited_pid = waitpid(pid, &status, WNOHANG);
    while (!waited_pid) {
        waited_pid = waitpid(pid, &status, WNOHANG);
        sleep(1);
    }

    FILE *fp;
    fp = fopen(path_res1, "r");
    init_array_from_file(fp, &res1);
    fclose(fp);
    fp = fopen(path_res2, "r");
    init_array_from_file(fp, &res2);
    fclose(fp);

    EXPECT_TRUE(compare_arrays(res1, res2, n_cols));

    free(res1);
    free(res2);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
