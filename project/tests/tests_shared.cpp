#include <dlfcn.h>
#include <gtest/gtest.h>
#include <fstream>
#include <sstream>

#define RANDOM_SEED 42
#define N_THREADS sysconf(_SC_NPROCESSORS_ONLN)

std::string readFileIntoString(const std::string &path) {
    std::ifstream input_file(path);
    if (!input_file.is_open()) {
        std::cerr << "Could not open the file - '"
                  << path << "'" << std::endl;
        exit(EXIT_FAILURE);
    }
    return std::string((std::istreambuf_iterator<char>(input_file)),
                       std::istreambuf_iterator<char>());
}

extern "C" {
#include "utils.h"
}

class SharedLibTest : public ::testing::Test {
 protected:
    void SetUp() {
        library = dlopen("project/shared_lib/libmat_sum_shared.so", RTLD_LAZY);
        mat_transpose = (bool (*)(matrix **new_mat,
                                  const matrix *old_mat)) dlsym(
                library, "transpose_matrix");
        matrix_sum_elems = (bool (*)(const matrix *mat, double *res, int axis,
                                     size_t threads_num)) dlsym(
                library, "matrix_sum_elems");
    }

    void TearDown() {
        dlclose(library);
    }

    void *library;

    bool (*mat_transpose)(matrix **new_mat, const matrix *old_mat);

    bool (*matrix_sum_elems)(const matrix *mat, double *res, int axis,
                             size_t threads_num);
};

TEST_F(SharedLibTest, input_test_1) {
    matrix *mat;
    double *correct_arr;
    FILE *fp;
    fp = fopen("../project/tests/test_data/input1.txt", "r");
    bool success = init_matrix_from_file(fp, &mat);
    fclose(fp);
    EXPECT_TRUE(success);
    fp = fopen("../project/tests/test_data/output1.txt", "r");
    size_t size = init_array_from_file(fp, &correct_arr);
    fclose(fp);
    EXPECT_EQ(size, mat->n_cols);
    auto *calculated_arr = (double *) malloc(mat->n_cols * sizeof(double));

    success = matrix_sum_elems(mat, calculated_arr, 0, N_THREADS);
    EXPECT_TRUE(success);
    EXPECT_TRUE(compare_arrays(calculated_arr, correct_arr, size));
    fp = fopen("../project/tests/test_data/test_output1.txt", "w");
    print_array(calculated_arr, size, fp);
    fclose(fp);
    std::string to_test = readFileIntoString(
            "../project/tests/test_data/test_output1.txt");
    std::string test = readFileIntoString(
            "../project/tests/test_data/output1.txt");
    EXPECT_EQ(test, to_test);

    del_matrix(mat);
    free(calculated_arr);
    free(correct_arr);
}

TEST_F(SharedLibTest, input_test_2) {
    matrix *mat;
    double *correct_arr;
    FILE *fp;
    fp = fopen("../project/tests/test_data/input2.txt", "r");
    bool success = init_matrix_from_file(fp, &mat);
    fclose(fp);
    EXPECT_TRUE(success);
    fp = fopen("../project/tests/test_data/output2.txt", "r");
    size_t size = init_array_from_file(fp, &correct_arr);
    fclose(fp);
    EXPECT_EQ(size, mat->n_cols);
    auto *calculated_arr = (double *) malloc(mat->n_cols * sizeof(double));
    success = matrix_sum_elems(mat, calculated_arr, 0, N_THREADS);
    EXPECT_TRUE(success);
    EXPECT_TRUE(compare_arrays(calculated_arr, correct_arr, size));

    del_matrix(mat);
    free(calculated_arr);
    free(correct_arr);
}

TEST_F(SharedLibTest, sum_elems_null_inputs) {
    matrix *mat = NULL;
    double *res1 = NULL;
    bool success = matrix_sum_elems(mat, res1, 0, N_THREADS);
    EXPECT_FALSE(success);
    free(res1);
    del_matrix(mat);
}

TEST_F(SharedLibTest, sum_elems_invalid_axis) {
    const size_t n_cols = 5;
    const size_t n_rows = 2;
    matrix *mat;
    auto *res1 = (double *) malloc(n_cols * sizeof(double));

    init_random_matrix(&mat, n_rows, n_cols, RANDOM_SEED);
    bool success = matrix_sum_elems(mat, res1, 2, N_THREADS);
    EXPECT_FALSE(success);
    success = matrix_sum_elems(mat, res1, -1, N_THREADS);
    EXPECT_FALSE(success);

    free(res1);
    del_matrix(mat);
}

TEST_F(SharedLibTest, sum_elems_invalid_nthreads) {
    const size_t n_cols = 5;
    const size_t n_rows = 2;
    matrix *mat;
    auto *res1 = (double *) malloc(n_cols * sizeof(double));

    init_random_matrix(&mat, n_rows, n_cols, RANDOM_SEED);
    bool success = matrix_sum_elems(mat, res1, 0, 0);
    EXPECT_FALSE(success);

    free(res1);
    del_matrix(mat);
}


TEST_F(SharedLibTest, sum_elems_single_col) {
    const size_t n_cols = 1;
    const size_t n_rows = 20;
    matrix *mat;
    auto *res1 = (double *) malloc(n_cols * sizeof(double));
    auto *res2 = (double *) malloc(n_rows * sizeof(double));

    init_random_matrix(&mat, n_rows, n_cols, RANDOM_SEED);
    bool success = matrix_sum_elems(mat, res1, 0, N_THREADS);
    EXPECT_TRUE(success);
    success = matrix_sum_elems(mat, res2, 1, N_THREADS);
    EXPECT_TRUE(success);

    del_matrix(mat);
    free(res1);
    free(res2);
}

TEST_F(SharedLibTest, sum_elems_single_row) {
    const size_t n_cols = 5;
    const size_t n_rows = 1;
    matrix *mat;
    auto *res1 = (double *) malloc(n_cols * sizeof(double));
    auto *res2 = (double *) malloc(n_rows * sizeof(double));

    init_random_matrix(&mat, n_rows, n_cols, RANDOM_SEED);
    bool success = matrix_sum_elems(mat, res1, 0, N_THREADS);
    EXPECT_TRUE(success);
    success = matrix_sum_elems(mat, res2, 1, N_THREADS);
    EXPECT_TRUE(success);

    del_matrix(mat);
    free(res1);
    free(res2);
}

TEST_F(SharedLibTest, sum_elems_zero_size) {
    const size_t n_cols = 5;
    const size_t n_rows = 2;
    matrix *mat;
    init_random_matrix(&mat, n_rows, n_cols, RANDOM_SEED);
    auto *res1 = (double *) malloc(n_cols * sizeof(double));
    mat->n_rows = 0;
    bool success = matrix_sum_elems(mat, res1, 0, N_THREADS);
    EXPECT_FALSE(success);
    mat->n_rows = n_rows;

    del_matrix(mat);
    free(res1);
}

TEST_F(SharedLibTest, test_transposing_null_inputs) {
    matrix *mat = NULL;
    matrix *mat_t = NULL;
    bool success = mat_transpose(&mat_t, mat);
    EXPECT_FALSE(success);

    del_matrix(mat);
    del_matrix(mat_t);
}

TEST_F(SharedLibTest, test_transposing) {
    matrix *mat;
    matrix *mat_t;
    const size_t n_cols = 1000;
    const size_t n_rows = 2000;
    auto *res1 = (double *) malloc(n_cols * sizeof(double));
    auto *res2 = (double *) malloc(n_cols * sizeof(double));
    bool success = init_random_matrix(&mat, n_rows, n_cols, RANDOM_SEED);
    EXPECT_TRUE(success);
    success = mat_transpose(&mat_t, mat);
    EXPECT_TRUE(success);
    success = matrix_sum_elems(mat, res1, 0, N_THREADS);
    EXPECT_TRUE(success);
    success = matrix_sum_elems(mat_t, res2, 1, N_THREADS);
    EXPECT_TRUE(success);
    EXPECT_TRUE(compare_arrays(res1, res2, n_cols));

    free(res1);
    free(res2);
    del_matrix(mat);
    del_matrix(mat_t);
}

TEST_F(SharedLibTest, test_transposing_single_row) {
    matrix *mat;
    const size_t n_cols = 5;
    const size_t n_rows = 1;
    auto *res1 = (double *) malloc(n_cols * sizeof(double));
    auto *res2 = (double *) malloc(n_cols * sizeof(double));
    bool success = init_random_matrix(&mat, n_rows, n_cols, RANDOM_SEED);
    EXPECT_TRUE(success);
    matrix *mat_t;
    success = mat_transpose(&mat_t, mat);
    EXPECT_TRUE(success);
    success = matrix_sum_elems(mat, res1, 0, N_THREADS);
    EXPECT_TRUE(success);
    success = matrix_sum_elems(mat_t, res2, 1, N_THREADS);
    EXPECT_TRUE(success);
    EXPECT_TRUE(compare_arrays(res1, res2, n_cols));

    free(res1);
    free(res2);
    del_matrix(mat);
    del_matrix(mat_t);
}

TEST_F(SharedLibTest, test_transposing_single_col) {
    matrix *mat;
    const size_t n_cols = 1;
    const size_t n_rows = 5;
    auto *res1 = (double *) malloc(n_cols * sizeof(double));
    auto *res2 = (double *) malloc(n_cols * sizeof(double));
    bool success = init_random_matrix(&mat, n_rows, n_cols, RANDOM_SEED);
    EXPECT_TRUE(success);
    matrix *mat_t;
    success = mat_transpose(&mat_t, mat);
    EXPECT_TRUE(success);
    success = matrix_sum_elems(mat, res1, 0, N_THREADS);
    EXPECT_TRUE(success);
    success = matrix_sum_elems(mat_t, res2, 1, N_THREADS);
    EXPECT_TRUE(success);
    EXPECT_TRUE(compare_arrays(res1, res2, n_cols));

    free(res1);
    free(res2);
    del_matrix(mat);
    del_matrix(mat_t);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
